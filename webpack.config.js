const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

var distDir = path.resolve(__dirname, 'build');

module.exports = {
	target: 'node',
	node: {
		__dirname: false
	},
	entry: [ './src/index.ts' ],
	output: {
		path: distDir,
		filename: 'index.js'
	},
	resolve: {
		extensions: [ '.js', '.ts', '.tsx', 'jsx' ]
	},
	module: {
		rules: [
			{
				test: /\.node$/,
				loader: 'node-loader'
			},
			{ test: /\.ts?$/, loader: 'ts-loader' }
		]
	},
	plugins: [ new CleanWebpackPlugin() ]
};
