import e from 'express';
const https = require('https');
const app: e.Application = e();
// Certificate

app.get('/',(req, res) => {
	res.send('Hello there !');
});

const httpsServer = https.createServer(app);

httpsServer.listen(8080, () => {
	console.log('HTTPS Server running on port 8080');
});